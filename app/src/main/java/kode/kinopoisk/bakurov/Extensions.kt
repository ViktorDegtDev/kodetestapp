package kode.kinopoisk.bakurov

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.*

fun View.gone() {
    visibility = View.GONE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.snackbar(message: String) = Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()

fun ViewGroup.inflate(layoutId: Int, inflater: LayoutInflater = LayoutInflater.from(context), attachToRoot: Boolean = false): View = inflater.inflate(layoutId, this, attachToRoot)

@Suppress("UNCHECKED_CAST") fun <T : View> Activity.findView(id: Int, action: T.() -> Unit = {}): T {
    val view = findViewById(id) as T
    action(view)
    return view
}

@Suppress("UNCHECKED_CAST") fun <T : View> View.findView(id: Int, action: T.() -> Unit = {}): T {
    val view = findViewById(id) as T
    action(view)
    return view
}

@Suppress("UNCHECKED_CAST") fun <T : View> View.findViewOptional(id: Int, action: T.() -> Unit = {}): T? {
    val view = findViewById(id) as T?
    if (view != null) action(view)
    return view
}

inline fun <reified T : Activity> Context.startActivity() = startActivity(IntentFor<T>(this))

inline fun <reified T : Activity> Context.startActivity(extras: Intent.() -> Unit) = startActivity(IntentFor<T>(this).apply { extras(this) })

inline fun <reified T : Activity> Fragment.startActivity(): Unit = context.startActivity<T>()

inline fun <reified T : Activity> Fragment.startActivity(extras: Intent.() -> Unit): Unit = context.startActivity<T> { extras() }

inline fun <reified T : Any> IntentFor(context: Context) = Intent(context, T::class.java)

fun Int.toDp(context: Context) = (context.resources.displayMetrics.density * this).toInt()