package kode.kinopoisk.bakurov

import kode.kinopoisk.bakurov.models.*
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface KinopoiskApi {
    @GET("getTodayFilms") fun getTodayFilms(): Observable<Films>
    @GET("getCinemas") fun getCinemas(@Query("cityID") id: Int)
    @GET("getFilm") fun getFilm(@Query("filmID") id: Int): Observable<Film>
    @GET("getSeance") fun getSeances(@Query("cityID") cityId: Int, @Query("filmID") filmId: Int): Observable<FilmSeances>
}