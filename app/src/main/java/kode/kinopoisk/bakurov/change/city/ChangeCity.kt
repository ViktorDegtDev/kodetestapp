package kode.kinopoisk.bakurov.change.city

import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import com.jakewharton.rxbinding.widget.textChanges
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.utils.FastAdapterUIUtils
import com.mikepenz.fastadapter.utils.ViewHolderFactory
import com.mikepenz.materialize.util.UIUtils
import com.rengwuxian.materialedittext.MaterialEditText
import kode.kinopoisk.bakurov.R
import kode.kinopoisk.bakurov.findView
import kode.kinopoisk.bakurov.models.City
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.customView
import org.jetbrains.anko.recyclerview.v7.recyclerView
import rx.Observable
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit

class ChangeCityDialog : BottomSheetDialogFragment() {
    companion object {
        @JvmStatic fun newInstance(cities: List<City>): ChangeCityDialog {
            val dialog = ChangeCityDialog()
            dialog.arguments = Bundle().apply { putParcelableArrayList("cities", ArrayList(cities)) }
            return dialog
        }
    }

    private val itemAdapter = FastItemAdapter<CityItem>()
    private var onSelect: ((City) -> Unit)? = null
    private var textChangesSubscription: Subscription? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        itemAdapter
                .withFilterPredicate { item, sequence -> item.city.name.contains(sequence.toString(), true).not() }
                .withOnClickListener { view, adapter, item, pos ->
                    textChangesSubscription?.unsubscribe()
                    onSelect?.invoke(item.city)
                    dismiss()
                    false
                }
        Observable.from(arguments.getParcelableArrayList<City>("cities"))
                .subscribeOn(Schedulers.newThread())
                .map { CityItem(it) }
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { itemAdapter.set(it) }
    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        dialog.setContentView(context.UI {
            linearLayout {
                layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
                orientation = LinearLayout.VERTICAL

                val materialEditText = customView<MaterialEditText> {
                    hint = "Введите название города"
                    setFloatingLabel(MaterialEditText.FLOATING_LABEL_NORMAL)
                }.lparams(width = matchParent, height = wrapContent) {
                    horizontalMargin = dip(16)
                }

                recyclerView {
                    layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    adapter = itemAdapter
                }.lparams(width = matchParent, height = matchParent) {
                    topMargin = dip(8)
                }

                textChangesSubscription = materialEditText
                        .textChanges()
                        .debounce(500, TimeUnit.MILLISECONDS)
                        .subscribe { itemAdapter.filter(it) }
            }
        }.view)
    }

    fun onSelect(action: (City) -> Unit): ChangeCityDialog {
        onSelect = action
        return this
    }
}

class CityItem(val city: City) : AbstractItem<CityItem, CityItem.Holder>() {
    companion object {
        val factory by lazy { CityItem.Factory() }
    }

    override fun getLayoutRes() = 0
    override fun getType() = R.id.city_item_id
    override fun getFactory() = Companion.factory

    override fun getViewHolder(parent: ViewGroup): Holder {
        return super.getViewHolder(parent.context.UI {
            frameLayout {
                layoutParams = ViewGroup.LayoutParams(matchParent, wrapContent)

                textView {
                    id = R.id.city_item_city_name_id
                    gravity = Gravity.CENTER_VERTICAL
                    minHeight = dip(48)
                    maxLines = 2
                    textSizeDimen = R.dimen.city_item_city_name_text_size
                    textColor = ContextCompat.getColor(context, R.color.md_grey_900)
                    horizontalPadding = dip(16)
                    verticalPadding = dip(8)
                    UIUtils.setBackground(this, FastAdapterUIUtils.getSelectableBackground(
                            context,
                            ContextCompat.getColor(context, R.color.md_grey_500),
                            true
                    ))
                }.lparams(width = matchParent, height = wrapContent)
            }
        }.view)
    }

    override fun bindView(holder: Holder) {
        super.bindView(holder)
        holder.cityName.text = city.name
    }

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cityName = itemView.findView<TextView>(R.id.city_item_city_name_id)
    }

    class Factory : ViewHolderFactory<Holder> {
        override fun create(v: View) = Holder(v)
    }
}