package kode.kinopoisk.bakurov.common

import android.content.Context
import android.os.Bundle
import android.view.*
import com.hannesdorfmann.mosby.mvp.MvpFragment
import com.hannesdorfmann.mosby.mvp.MvpView
import kode.kinopoisk.bakurov.inflate

abstract class RxMvpFragment<V : MvpView, P : RxMvpPresenter<V>> : MvpFragment<V, P>() {

    override fun onAttach(context: Context) {
        super.onAttach(context)
        presenter?.onAttach()
    }

    open fun getLayoutRes(): Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(getLayoutRes(), inflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter?.onCreate()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter?.onViewCreated()
    }

    override fun onStart() {
        super.onStart()
        presenter?.onStart()
    }

    override fun onResume() {
        super.onResume()
        presenter?.onResume()
    }

    override fun onPause() {
        super.onPause()
        presenter?.onPause()
    }

    override fun onStop() {
        super.onStop()
        presenter?.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter?.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.onDestroy()
    }

    override fun onDetach() {
        super.onDetach()
        presenter?.onDetach()
    }
}