package kode.kinopoisk.bakurov.common

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby.mvp.MvpView
import com.orhanobut.logger.Logger
import com.pushtorefresh.storio.sqlite.StorIOSQLite
import com.pushtorefresh.storio.sqlite.operations.get.PreparedGet
import com.pushtorefresh.storio.sqlite.queries.Query
import com.trello.rxlifecycle.*
import kode.kinopoisk.bakurov.KinopoiskApi
import kode.kinopoisk.bakurov.tags.LoggerTags.STORIO
import rx.Observable
import rx.lang.kotlin.BehaviorSubject
import javax.inject.Inject

abstract class RxMvpPresenter<V : MvpView> : MvpBasePresenter<V>(), FragmentLifecycleProvider {
    @Inject lateinit var kinopoiskApi: KinopoiskApi
    @Inject lateinit var storio: StorIOSQLite
    private val lifecycleSubject = BehaviorSubject<FragmentEvent>()

    override fun lifecycle(): Observable<FragmentEvent> = lifecycleSubject.asObservable()
    override fun <T : Any?> bindUntilEvent(event: FragmentEvent): LifecycleTransformer<T> = RxLifecycle.bindUntilEvent(lifecycleSubject, event)
    override fun <T : Any?> bindToLifecycle(): LifecycleTransformer<T> = RxLifecycle.bindFragment(lifecycleSubject)
    fun onAttach() = lifecycleSubject.onNext(FragmentEvent.ATTACH)
    fun onCreate() = lifecycleSubject.onNext(FragmentEvent.CREATE)
    fun onViewCreated() = lifecycleSubject.onNext(FragmentEvent.CREATE_VIEW)
    fun onStart() = lifecycleSubject.onNext(FragmentEvent.START)
    fun onResume() = lifecycleSubject.onNext(FragmentEvent.RESUME)
    fun onPause() = lifecycleSubject.onNext(FragmentEvent.PAUSE)
    fun onStop() = lifecycleSubject.onNext(FragmentEvent.STOP)
    fun onDestroyView() = lifecycleSubject.onNext(FragmentEvent.DESTROY_VIEW)
    fun onDestroy() = lifecycleSubject.onNext(FragmentEvent.DESTROY)
    fun onDetach() = lifecycleSubject.onNext(FragmentEvent.DETACH)

    protected fun <E> storioPut(entry: E) {
        storio.put()
                .`object`(entry)
                .prepare()
                .asRxSingle()
                .subscribe({
                    Logger.t(STORIO).d("Put successfully\nUpdated: ${it.numberOfRowsUpdated()}")
                }, {
                    Logger.t(STORIO).e("Put Fail $it")
                    it.printStackTrace()
                })
    }

    protected fun <E> storioPut(entries: List<E>) {
        storio.put()
                .objects(entries)
                .prepare()
                .asRxSingle()
                .subscribe({
                    Logger.t(STORIO).d("Put successfully\nUpdated: ${it.numberOfUpdates()}")
                }, {
                    Logger.t(STORIO).e("Put Fail $it")
                    it.printStackTrace()
                })
    }

    inline fun <reified E : Any> PreparedGet.Builder.listWithQuery(query: Query): Observable<List<E>> {
        return listOfObjects<E>(E::class.java)
                .withQuery(query)
                .prepare()
                .asRxObservable()
                .take(1)
    }

    inline fun <reified E : Any> PreparedGet.Builder.withQuery(query: Query): Observable<E> {
        return `object`(E::class.java)
                .withQuery(query)
                .prepare()
                .asRxObservable()
                .take(1)
    }
}