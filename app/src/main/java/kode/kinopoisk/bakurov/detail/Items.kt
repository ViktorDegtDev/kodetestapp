package kode.kinopoisk.bakurov.detail

import android.graphics.Rect
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.support.v7.widget.*
import android.view.*
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.mikepenz.community_material_typeface_library.CommunityMaterial
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.utils.FastAdapterUIUtils
import com.mikepenz.fastadapter.utils.ViewHolderFactory
import com.mikepenz.iconics.IconicsDrawable
import com.mikepenz.iconics.typeface.IIcon
import com.mikepenz.materialize.util.UIUtils
import jp.wasabeef.glide.transformations.CropTransformation
import kode.kinopoisk.bakurov.R
import kode.kinopoisk.bakurov.findView
import kode.kinopoisk.bakurov.models.Cinema
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.recyclerview.v7.recyclerView
import java.text.*
import java.util.*

abstract class AbstractCardItem : AbstractItem<AbstractCardItem, AbstractCardItem.Holder>() {
    companion object {
        private val factory by lazy { AbstractCardItem.Factory() }
    }

    override fun getLayoutRes() = 0
    override fun getType() = R.id.detail_film_item_id
    override fun getFactory() = Companion.factory

    override fun getViewHolder(parent: ViewGroup): Holder {
        return super.getViewHolder(parent.context.UI {
            cardView {
                layoutParams = ViewGroup.MarginLayoutParams(matchParent, wrapContent).apply { bottomMargin = dip(8) }
                cardElevation = dip(2).toFloat()
                radius = dip(1).toFloat()
            }
        }.view)
    }

    fun bindView(holder: Holder, init: _LinearLayout.() -> Unit) {
        (holder.itemView as ViewGroup).apply {
            removeAllViews()
            linearLayout {
                orientation = LinearLayout.VERTICAL
                verticalPadding = dip(4)
                UIUtils.setBackground(this, FastAdapterUIUtils.getSelectableBackground(
                        context,
                        ContextCompat.getColor(context, R.color.md_grey_500),
                        true
                ))
                init()
            }
        }
    }

    protected open fun _LinearLayout.createContentView(title: String, icon: IIcon? = null, optionalInit: TextView.() -> Unit = {}) {
        textView {
            if (icon != null) {
                setCompoundDrawablesRelative(
                        IconicsDrawable(context)
                                .icon(icon)
                                .colorRes(R.color.md_grey_500)
                                .paddingDp(3)
                                .sizeDp(24),
                        null, null, null
                )
                compoundDrawablePadding = dip(16)
            }
            textColor = ContextCompat.getColor(context, R.color.md_grey_900)
            textSizeDimen = R.dimen.detail_view_card_content_text_size
            text = title
            optionalInit()
        }.lparams(width = matchParent, height = wrapContent) {
            leftMargin = dip(24)
            rightMargin = dip(16)
            verticalMargin = dip(4)
        }
    }

    protected open fun _LinearLayout.createHeaderView(title: String, optionalInit: TextView.() -> Unit = {}) {
        textView {
            textColor = ContextCompat.getColor(context, R.color.md_grey_700)
            textSizeDimen = R.dimen.detail_view_card_header_text_size
            text = title
            optionalInit()
        }.lparams(width = matchParent, height = wrapContent) {
            horizontalMargin = dip(16)
            verticalMargin = dip(4)
        }
    }

    class Holder(card: CardView) : RecyclerView.ViewHolder(card)

    class Factory : ViewHolderFactory<Holder> {
        override fun create(v: View) = Holder(v as CardView)
    }
}

class CommonInfoItem(
        //region Fields
        val nameEn: String,
        val year: Int,
        val genres: List<String>,
        val ratingMpaa: String?,
        val country: String,
        val filmLength: String?
        //endregion
) : AbstractCardItem() {
    override fun bindView(holder: Holder) {
        super.bindView(holder)
        bindView(holder) {
            createContentView("$nameEn ($year)", CommunityMaterial.Icon.cmd_format_title)
            createContentView("${genres.joinToString()} (${ratingMpaa ?: "-"})", CommunityMaterial.Icon.cmd_movie)
            createContentView(country, CommunityMaterial.Icon.cmd_flag)
            createContentView(filmLength?.toString() ?: "-", CommunityMaterial.Icon.cmd_clock)
        }
    }
}

class DescriptionItem(val desc: String) : AbstractCardItem() {
    override fun bindView(holder: Holder) {
        super.bindView(holder)
        bindView(holder) {
            createHeaderView("Описание")
            createContentView(desc)
        }
    }
}

class RentItem(
        //region Fields
        val premiereRu: String?,
        val premiereWorld: String?,
        val distributors: String?,
        val premiereWorldCountry: String?
        //endregion
) : AbstractCardItem() {
    override fun bindView(holder: Holder) {
        super.bindView(holder)
        bindView(holder) {
            createHeaderView("Даты выхода")
            createContentView("Премьера(РФ): ${premiereRu?.toFormattedDate() ?: "-"} (${distributors ?: "-"})")
            createContentView("Премьера(Мир): ${premiereWorld?.toFormattedDate() ?: "-"} (${premiereWorldCountry ?: "-"})")
        }
    }

    @Suppress("NOTHING_TO_INLINE")
    private inline fun String.toFormattedDate(): String {
        val parsed = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).parse(this)
        return SimpleDateFormat("d MMMM yyyy", Locale.getDefault()).format(parsed)
    }
}

class BudgetItem(
        //region Fields
        val budget: Int?,
        val grossRu: Int?,
        val grossUsa: Int?,
        val grossWorld: Int?
        //endregion
) : AbstractCardItem() {
    companion object {
        private val formatter = DecimalFormat("###,###", DecimalFormatSymbols().apply { groupingSeparator = ' ' })
    }

    override fun bindView(holder: Holder) {
        super.bindView(holder)
        bindView(holder) {
            createHeaderView("Бюджет и сборы")
            if (budget != null) createContentView("Бюджет: $${formatter.format(budget)}")
            createContentView("Сборы:")
            if (grossRu != null) createContentView("\t\t...РФ: $${formatter.format(grossRu)}")
            if (grossUsa != null) createContentView("\t\t...США: $${formatter.format(grossUsa)}")
            if (grossWorld != null) createContentView("\t\t...Мир: $${formatter.format(grossWorld)}")
        }
    }
}

class GalleryItem(val gallery: List<String>) : AbstractCardItem() {
    private val fastItemAdapter by lazy {
        val adapter = FastItemAdapter<InternalGalleryItem>()
        adapter.set(gallery.map { InternalGalleryItem(it) })
        adapter
    }

    override fun getType() = R.id.gallery_card_item_id

    override fun bindView(holder: Holder) {
        super.bindView(holder)
        bindView(holder) {
            createHeaderView("Галерея")
            recyclerView {
                id = R.id.gallery_card_rv_id

                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                addItemDecoration(object : RecyclerView.ItemDecoration() {
                    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                        val position = parent.getChildAdapterPosition(view)
                        val left = if (position == 0) 4 else 2
                        val right = if (position == parent.adapter.itemCount - 1) 4 else 2

                        outRect.top = dip(2)
                        outRect.bottom = dip(2)
                        outRect.left = dip(left)
                        outRect.right = dip(right)
                    }
                })
                adapter = fastItemAdapter
            }.lparams(width = matchParent, height = dip(100))
        }
    }

    private class InternalGalleryItem(val image: String) : AbstractItem<InternalGalleryItem, InternalGalleryItem.Holder>() {
        companion object {
            private val factory = InternalGalleryItem.Factory()
        }

        override fun getLayoutRes() = 0
        override fun getType() = R.id.internal_gallery_item_id
        override fun getFactory() = InternalGalleryItem.Companion.factory

        override fun getViewHolder(parent: ViewGroup): Holder {
            return super.getViewHolder(parent.context.UI {
                frameLayout {
                    lparams(wrapContent, matchParent)

                    imageView {
                        id = R.id.internal_gallery_item_image_view_id
                        minimumWidth = dip(90)
                    }.lparams(width = wrapContent, height = matchParent)
                }
            }.view)
        }

        override fun bindView(holder: Holder) {
            super.bindView(holder)
            val context = holder.image.context
            Glide.with(context)
                    .load(image)
                    .asBitmap()
                    .placeholder(R.color.md_grey_400)
                    .animate(android.R.anim.fade_in)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .transform(CropTransformation(context), CenterCrop(context))
                    .into(holder.image)
        }

        private class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val image = itemView.findView<ImageView>(R.id.internal_gallery_item_image_view_id)
        }

        private class Factory : ViewHolderFactory<Holder> {
            override fun create(v: View) = Holder(v)
        }
    }
}

class CinemasCommonItem(
        val cityName: String,
        val cinemas: Int,
        val seances: Int
) : AbstractCardItem() {
    override fun getType() = R.id.cinema_card_item_id

    override fun bindView(holder: Holder) {
        super.bindView(holder)
        bindView(holder) {
            createHeaderView("Где посмотреть")
            createContentView(cityName) {
                setTypeface(typeface, Typeface.BOLD)
                textSizeDimen = R.dimen.detail_view_card_seance_cinema_text_size
            }
            createContentView("Кинотеатры $cinemas")
            createContentView("Сеансы $seances")
        }
    }
}

class CinemaSeancesItem(val cinema: Cinema) : AbstractCardItem() {
    override fun getType() = R.id.cinema_card_item_id

    override fun bindView(holder: Holder) {
        super.bindView(holder)
        bindView(holder) {
            createHeaderView(cinema.name) {
                textSizeDimen = R.dimen.detail_view_card_seance_cinema_text_size
                setTypeface(typeface, Typeface.BOLD)
                textColor = ContextCompat.getColor(context, R.color.md_grey_900)
            }
            createContentView(cinema.address) {
                textSizeDimen = R.dimen.detail_view_card_content_text_size
            }

            if (cinema.seances != null)
                createSeancesTimes(cinema.seances)

            if (cinema.seances3D != null) {
                createContentView("Сеансы 3D")
                createSeancesTimes(cinema.seances3D)
            }
        }
    }

    private fun _LinearLayout.createSeancesTimes(seances: List<String>) {
        gridLayout {
            columnCount = 5
            seances.forEach {
                textView {
                    text = it
                    textSizeDimen = R.dimen.detail_view_card_seance_time_text_size
                    textColor = ContextCompat.getColor(context, R.color.md_grey_900)
                    backgroundResource = R.drawable.seance_time_background
                    gravity = Gravity.CENTER
                }.lparams {
                    verticalMargin = dip(2)
                    horizontalMargin = dip(4)
                }
            }
        }.lparams(matchParent, wrapContent) {
            verticalMargin = dip(4)
            leftMargin = dip(24)
            rightMargin = dip(16)
        }
    }
}