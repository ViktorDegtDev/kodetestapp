package kode.kinopoisk.bakurov.di

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.pushtorefresh.storio.sqlite.StorIOSQLite
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite
import dagger.Module
import dagger.Provides
import kode.kinopoisk.bakurov.models.*
import rx.schedulers.Schedulers
import javax.inject.Singleton

@Module class DatabaseModule {
    @Provides @Singleton fun provideStorIO(helper: SQLiteOpenHelper): StorIOSQLite = DefaultStorIOSQLite.builder()
            .sqliteOpenHelper(helper)
            .defaultScheduler(Schedulers.io())
            .addTypeMapping(Film::class.java, Film.storioTypeMapping())
            .addTypeMapping(Profile::class.java, Profile.storioTypeMapping())
            .addTypeMapping(Cinema::class.java, Cinema.storioTypeMapping())
            .addTypeMapping(FilmSeances::class.java, FilmSeances.storioTypeMapping())
            .addTypeMapping(City::class.java, City.storioTypeMapping())
            .build()

    @Provides fun provideSQLiteOpenHelper(context: Context): SQLiteOpenHelper {
        return object : SQLiteOpenHelper(context, "DB", null, 1) {
            override fun onCreate(database: SQLiteDatabase) {
                Film.createTable(database)
                Profile.createTable(database)
                Cinema.createTable(database)
                FilmSeances.createTable(database)
                City.createTable(database)
            }

            override fun onUpgrade(database: SQLiteDatabase, v1: Int, v2: Int) {
            }
        }
    }
}