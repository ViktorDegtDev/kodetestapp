package kode.kinopoisk.bakurov.di

import android.content.Context
import kode.kinopoisk.bakurov.MainActivityPresenter
import kode.kinopoisk.bakurov.detail.DetailFilmPresenter
import kode.kinopoisk.bakurov.today.films.TodayFilmsPresenter

object Injector {
    private lateinit  var mainComponent: MainComponent

    fun init(context: Context) {
        mainComponent = DaggerMainComponent.builder()
                .networkModule(NetworkModule())
                .appModule(AppModule(context))
                .databaseModule(DatabaseModule())
                .build()
    }

    fun inject(presenter: TodayFilmsPresenter) = mainComponent.inject(presenter)
    fun inject(presenter: DetailFilmPresenter) = mainComponent.inject(presenter)
    fun inject(presenter: MainActivityPresenter) = mainComponent.inject(presenter)
}