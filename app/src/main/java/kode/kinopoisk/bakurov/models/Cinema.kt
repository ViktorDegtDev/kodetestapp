package kode.kinopoisk.bakurov.models

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.*
import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping
import com.pushtorefresh.storio.sqlite.operations.delete.DefaultDeleteResolver
import com.pushtorefresh.storio.sqlite.operations.get.DefaultGetResolver
import com.pushtorefresh.storio.sqlite.operations.put.DefaultPutResolver
import com.pushtorefresh.storio.sqlite.queries.*
import java.lang.reflect.Type
import java.util.*

data class Cinema(
        val id: Int,
        val name: String,
        val address: String,
        val lon: Float?,
        val lat: Float?,
        val seances: List<String>?,
        val seances3D: List<String>?
) : Parcelable {
    companion object {
        @JvmStatic fun deserializer() = Deserializer()

        @JvmStatic fun storioTypeMapping(): SQLiteTypeMapping<Cinema> {
            return SQLiteTypeMapping.builder<Cinema>()
                    .putResolver(SimplePutResolver())
                    .getResolver(SimpleGetResolver())
                    .deleteResolver(SimpleDeleteResolver())
                    .build()
        }

        @JvmStatic fun createTable(database: SQLiteDatabase) {
            database.execSQL("CREATE TABLE ${Cinema.Table.TABLE_NAME} (" +
                    "${Cinema.Table.ID} INTEGER PRIMARY KEY," +
                    "${Cinema.Table.NAME} TEXT," +
                    "${Cinema.Table.ADDRESS} TEXT," +
                    "${Cinema.Table.LON} REAL," +
                    "${Cinema.Table.LAT} REAL," +
                    "${Cinema.Table.SEANCES} TEXT," +
                    "${Cinema.Table.SEANCES_3D} TEXT" +
                    ");")
        }

        @JvmField val CREATOR = object : Parcelable.Creator<Cinema> {
            override fun createFromParcel(source: Parcel) = Cinema(
                    id = source.readInt(),
                    name = source.readString(),
                    address = source.readString(),
                    lon = source.readSerializable() as? Float,
                    lat = source.readSerializable() as? Float,
                    seances = ArrayList<String>().apply { source.readList(this, String::class.java.classLoader) },
                    seances3D = ArrayList<String>().apply { source.readList(this, String::class.java.classLoader) }
            )

            override fun newArray(size: Int): Array<Cinema?> = arrayOfNulls(size)
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(id)
        dest.writeString(name)
        dest.writeString(address)
        dest.writeSerializable(lon)
        dest.writeSerializable(lat)
        dest.writeStringList(seances)
        dest.writeList(seances3D)
    }

    class Deserializer : JsonDeserializer<Cinema> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext) = json.asJsonObject.let {
            Cinema(
                    id = it["cinemaID"].asInt,
                    name = it["cinemaName"].asString,
                    address = it["address"].asString,
                    lon = it["lon"]?.asFloat,
                    lat = it["lat"]?.asFloat,
                    seances = it["seance"]?.asJsonArray?.map { it.asString },
                    seances3D = it["seance3D"]?.asJsonArray?.map { it.asString }
            )
        }
    }

    class SimplePutResolver : DefaultPutResolver<Cinema>() {
        override fun mapToInsertQuery(cinema: Cinema) = InsertQuery.builder()
                .table(Cinema.Table.TABLE_NAME)
                .build()

        override fun mapToUpdateQuery(cinema: Cinema) = UpdateQuery.builder()
                .table(Cinema.Table.TABLE_NAME)
                .where("${Cinema.Table.ID} = ${cinema.id}")
                .build()

        override fun mapToContentValues(cinema: Cinema): ContentValues {
            return ContentValues().apply {
                put(Cinema.Table.ID, cinema.id)
                put(Cinema.Table.NAME, cinema.name)
                put(Cinema.Table.ADDRESS, cinema.address)
                put(Cinema.Table.LON, cinema.lon)
                put(Cinema.Table.LAT, cinema.lat)
                put(Cinema.Table.SEANCES, cinema.seances?.joinToString(","))
                put(Cinema.Table.SEANCES_3D, cinema.seances3D?.joinToString(","))
            }
        }
    }

    class SimpleGetResolver : DefaultGetResolver<Cinema>() {
        override fun mapFromCursor(cursor: Cursor) = Cinema(
                id = cursor.getInt(cursor.getColumnIndex(Cinema.Table.ID)),
                name = cursor.getString(cursor.getColumnIndex(Cinema.Table.NAME)),
                address = cursor.getString(cursor.getColumnIndex(Cinema.Table.ADDRESS)),
                lon = cursor.getFloat(cursor.getColumnIndex(Cinema.Table.LON)),
                lat = cursor.getFloat(cursor.getColumnIndex(Cinema.Table.LAT)),
                seances = cursor.getString(cursor.getColumnIndex(Cinema.Table.SEANCES))?.split(","),
                seances3D = cursor.getString(cursor.getColumnIndex(Cinema.Table.SEANCES_3D))?.split(",")
        )
    }

    class SimpleDeleteResolver : DefaultDeleteResolver<Cinema>() {
        override fun mapToDeleteQuery(cinema: Cinema) = DeleteQuery.builder()
                .table(Cinema.Table.TABLE_NAME)
                .where("${Cinema.Table.ID} = ${cinema.id}")
                .build()
    }

    object Table {
        val TABLE_NAME = "CINEMAS"
        val ID = "ID"
        val NAME = "NAME"
        val ADDRESS = "ADDRESS"
        val LON = "LON"
        val LAT = "LAT"
        val SEANCES = "SEANCES"
        val SEANCES_3D = "SEANCES_3D"
    }
}