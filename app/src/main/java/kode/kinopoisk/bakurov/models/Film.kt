package kode.kinopoisk.bakurov.models

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.*
import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping
import com.pushtorefresh.storio.sqlite.StorIOSQLite
import com.pushtorefresh.storio.sqlite.operations.delete.DefaultDeleteResolver
import com.pushtorefresh.storio.sqlite.operations.get.GetResolver
import com.pushtorefresh.storio.sqlite.operations.put.*
import com.pushtorefresh.storio.sqlite.queries.*
import java.lang.reflect.Type
import java.util.*

data class Film(
        //region Fields
        val id: Int,
        val nameRu: String,
        val nameEn: String,
        val year: Int,
        val country: String,
        val posterBig: String,
        val posterSmall: String,
        val genres: List<String>,
        val cinemasCount: Int? = null,
        val filmLength: String? = null,
        val desc: String? = null,
        val videoUrl: String? = null,
        val reviewsCount: Int? = null,
        val ratingGoodReview: Float? = null,
        val ratingGoodReviewVoteCount: Int? = null,
        val rating: Float? = null,
        val ratingMPAA: String? = null,
        val ratingVoteCount: Int? = null,
        val ratingAwait: Float? = null,
        val ratingAwaitCount: Int? = null,
        val ratingIMDb: Float? = null,
        val ratingIMDbVoteCount: Int? = null,
        val distributors: String? = null,
        val premiereRu: String? = null,
        val premiereWorld: String? = null,
        val premiereWorldCountry: String? = null,
        val grossRu: Int? = null,
        val grossUsa: Int? = null,
        val grossWorld: Int? = null,
        val budget: Int? = null,
        val gallery: List<String>? = null,
        val triviaData: List<String>? = null,
        val creators: List<Profile>? = null
        //endregion
) : Parcelable {
    companion object {
        @JvmStatic fun deserializer() = Deserializer()
        @JvmStatic fun storioTypeMapping() = SQLiteTypeMapping.builder<Film>()
                .putResolver(RelationsPutResolver())
                .getResolver(RelationsGetResolver())
                .deleteResolver(SimpleDeleteResolver())
                .build()

        @JvmStatic fun createTable(database: SQLiteDatabase) {
            database.execSQL("CREATE TABLE ${Table.TABLE_NAME} (" +
                    "${Table.ID} INTEGER PRIMARY KEY," +
                    "${Table.NAME_RU} TEXT," +
                    "${Table.NAME_EN} TEXT," +
                    "${Table.YEAR} INTEGER," +
                    "${Table.CINEMAS_COUNT} INTEGER," +
                    "${Table.POSTER_BIG} TEXT," +
                    "${Table.POSTER_SMALL} TEXT," +
                    "${Table.FILM_LENGTH} TEXT," +
                    "${Table.COUNTRY} TEXT," +
                    "${Table.GENRES} TEXT," +
                    "${Table.DESC} TEXT," +
                    "${Table.VIDEO_URL} TEXT," +
                    "${Table.REVIEWS_COUNT} INTEGER," +
                    "${Table.RATING_GOOD_REVIEW} REAL," +
                    "${Table.RATING_GOOD_REVIEW_VOTE_COUNT} INTEGER," +
                    "${Table.RATING} REAL," +
                    "${Table.RATING_MPAA} TEXT," +
                    "${Table.RATING_VOTE_COUNT} INTEGER," +
                    "${Table.RATING_AWAIT} REAL," +
                    "${Table.RATING_AWAIT_COUNT} INTEGER," +
                    "${Table.RATING_IMDB} REAL," +
                    "${Table.RATING_IMDB_VOTE_COUNT} INTEGER," +
                    "${Table.DISTRIBUTORS} TEXT," +
                    "${Table.PREMIERE_RU} TEXT," +
                    "${Table.PREMIERE_WORLD} TEXT," +
                    "${Table.PREMIERE_WORLD_COUNTRY} TEXT," +
                    "${Table.GROSS_RU} INTEGER," +
                    "${Table.GROSS_USA} INTEGER," +
                    "${Table.GROSS_WORLD} INTEGER," +
                    "${Table.BUDGET} INTEGER," +
                    "${Table.GALLERY} TEXT," +
                    "${Table.TRIVIA_DATA} TEXT," +
                    "${Table.CREATORS} TEXT" +
                    ");")
        }

        @JvmField val CREATOR = object : Parcelable.Creator<Film> {
            override fun createFromParcel(source: Parcel) = Film(
                    id = source.readInt(),
                    nameRu = source.readString(),
                    nameEn = source.readString(),
                    year = source.readInt(),
                    country = source.readString(),
                    posterBig = source.readString(),
                    posterSmall = source.readString(),
                    genres = ArrayList<String>().apply { source.readStringList(this) },
                    cinemasCount = source.readSerializable() as? Int,
                    filmLength = source.readSerializable() as? String,
                    desc = source.readSerializable() as? String,
                    videoUrl = source.readSerializable() as? String,
                    reviewsCount = source.readSerializable() as? Int,
                    ratingGoodReview = source.readSerializable() as? Float,
                    ratingGoodReviewVoteCount = source.readSerializable() as? Int,
                    rating = source.readSerializable() as? Float,
                    ratingMPAA = source.readSerializable() as? String,
                    ratingVoteCount = source.readSerializable() as? Int,
                    ratingAwait = source.readSerializable() as? Float,
                    ratingAwaitCount = source.readSerializable() as? Int,
                    ratingIMDb = source.readSerializable() as? Float,
                    ratingIMDbVoteCount = source.readSerializable() as? Int,
                    distributors = source.readSerializable() as? String,
                    premiereRu = source.readSerializable() as? String,
                    premiereWorld = source.readSerializable() as? String,
                    premiereWorldCountry = source.readSerializable() as? String,
                    grossRu = source.readSerializable() as? Int,
                    grossUsa = source.readSerializable() as? Int,
                    grossWorld = source.readSerializable() as? Int,
                    budget = source.readSerializable() as? Int,
                    gallery = ArrayList<String>().apply { source.readStringList(this) },
                    triviaData = ArrayList<String>().apply { source.readStringList(this) },
                    creators = source.readParcelableArray(Profile::class.java.classLoader)?.toList() as? List<Profile>
            )

            override fun newArray(size: Int): Array<Film?> = arrayOfNulls(size)
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(nameRu)
        parcel.writeString(nameEn)
        parcel.writeInt(year)
        parcel.writeString(country)
        parcel.writeString(posterBig)
        parcel.writeString(posterSmall)
        parcel.writeStringList(genres)
        parcel.writeSerializable(cinemasCount)
        parcel.writeSerializable(filmLength)
        parcel.writeSerializable(desc)
        parcel.writeSerializable(videoUrl)
        parcel.writeSerializable(reviewsCount)
        parcel.writeSerializable(ratingGoodReview)
        parcel.writeSerializable(ratingGoodReviewVoteCount)
        parcel.writeSerializable(rating)
        parcel.writeSerializable(ratingMPAA)
        parcel.writeSerializable(ratingVoteCount)
        parcel.writeSerializable(ratingAwait)
        parcel.writeSerializable(ratingAwaitCount)
        parcel.writeSerializable(ratingIMDb)
        parcel.writeSerializable(ratingIMDbVoteCount)
        parcel.writeSerializable(distributors)
        parcel.writeSerializable(premiereRu)
        parcel.writeSerializable(premiereWorld)
        parcel.writeSerializable(premiereWorldCountry)
        parcel.writeSerializable(grossRu)
        parcel.writeSerializable(grossUsa)
        parcel.writeSerializable(grossWorld)
        parcel.writeSerializable(budget)
        parcel.writeStringList(gallery)
        parcel.writeStringList(triviaData)
        parcel.writeParcelableArray(creators?.toTypedArray(), flags)
    }

    override fun describeContents() = 0

    class Deserializer : JsonDeserializer<Film> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext) = json.asJsonObject.let {
            val ratingData = it["ratingData"]?.asJsonObject
            val rentData = it["rentData"]?.asJsonObject
            val budgetData = it["budgetData"]?.asJsonObject
            val id = (it["id"] ?: it["filmID"]).asInt
            Film(
                    id = id,
                    nameRu = it["nameRU"].asString,
                    nameEn = it["nameEN"].asString,
                    posterSmall = "https://kinopoisk.ru/images/${it["posterURL"]?.asString ?: "no-poster.gif"}",
                    posterBig = "https://kinopoisk.ru/images/film_big/$id.jpg",
                    year = it["year"].asInt,
                    cinemasCount = it["cinemaHallCount"]?.asInt,
                    filmLength = it["filmLength"]?.asString,
                    country = it["country"].asString,
                    genres = it["genre"].asString.split(",").map { it.trim() },
                    desc = it["description"]?.asString,
                    videoUrl = it["videoURL"]?.asString,
                    reviewsCount = it["reviewsCount"]?.asInt,
                    rating = Regex("\\d+\\.\\d+").find((ratingData?.get("rating") ?: it["rating"]).asString)?.value?.toFloat(),
                    ratingMPAA = it["ratingMPAA"]?.asString,
                    ratingGoodReview = ratingData?.get("ratingGoodReview")?.asString?.replace("%", "")?.toFloat(),
                    ratingGoodReviewVoteCount = ratingData?.get("ratingGoodReviewVoteCount")?.asInt,
                    ratingVoteCount = ratingData?.get("ratingVoteCount")?.asString?.replace(" ", "")?.toInt()
                            ?: it["rating"].asString.let { it.substring(it.indexOf("(") + 1, it.indexOf(")")) }.replace(" ", "").toInt(),
                    ratingAwait = ratingData?.get("ratingAwait")?.asString?.replace("%", "")?.toFloat(),
                    ratingAwaitCount = ratingData?.get("ratingAwaitCount")?.asString?.replace(" ", "")?.toInt(),
                    ratingIMDb = ratingData?.get("ratingIMDb")?.asString?.toFloat(),
                    ratingIMDbVoteCount = ratingData?.get("ratingIMDbVoteCount")?.asString?.replace(" ", "")?.toInt(),
                    premiereRu = rentData?.get("premiereRU")?.asString,
                    distributors = rentData?.get("Distributors")?.asString,
                    premiereWorld = rentData?.get("premiereWorld")?.asString,
                    premiereWorldCountry = rentData?.get("premiereWorldCountry")?.asString,
                    grossRu = budgetData?.get("grossRU")?.asString?.replace(" ", "")?.toInt(),
                    grossUsa = budgetData?.get("grossUSA")?.asString?.replace(" ", "")?.toInt(),
                    grossWorld = budgetData?.get("grossWorld")?.asString?.replace(" ", "")?.toInt(),
                    budget = budgetData?.get("budget")?.asString?.replace(" ", "")?.let { Regex("\\d+").find(it) }?.value?.toInt(),
                    gallery = it["gallery"]?.asJsonArray?.map { "https://kinopoisk.ru/images/${it.asJsonObject["preview"].asString}" },
                    triviaData = it["triviaData"]?.asJsonArray?.map { it.asString },
                    creators = it["creators"]?.asJsonArray?.map { it.asJsonArray }?.flatten()?.map { context.deserialize<Profile>(it, Profile::class.java) }
            )
        }
    }

    class RelationsPutResolver : PutResolver<Film>() {
        override fun performPut(storio: StorIOSQLite, film: Film): PutResult {
            val filmPutResult = storio.put()
                    .`object`(film)
                    .withPutResolver(SimplePutResolver())
                    .prepare()
                    .executeAsBlocking()
            if (film.creators != null) {
                val creatorsPutResults = storio.put()
                        .objects(film.creators)
                        .prepare()
                        .executeAsBlocking()
                return PutResult.newUpdateResult(
                        filmPutResult.numberOfRowsUpdated() ?: 0 + creatorsPutResults.numberOfUpdates(),
                        setOf(Film.Table.TABLE_NAME, Profile.Table.TABLE_NAME)
                )
            }
            return filmPutResult
        }
    }

    class SimplePutResolver : DefaultPutResolver<Film>() {
        override fun mapToContentValues(film: Film) = ContentValues().apply {
            put(Table.ID, film.id)
            put(Table.NAME_RU, film.nameRu)
            put(Table.NAME_EN, film.nameEn)
            put(Table.YEAR, film.year)
            put(Table.CINEMAS_COUNT, film.cinemasCount)
            put(Table.POSTER_BIG, film.posterBig)
            put(Table.POSTER_SMALL, film.posterSmall)
            put(Table.FILM_LENGTH, film.filmLength)
            put(Table.COUNTRY, film.country)
            put(Table.GENRES, film.genres.joinToString(","))
            put(Table.DESC, film.desc)
            put(Table.VIDEO_URL, film.videoUrl)
            put(Table.REVIEWS_COUNT, film.reviewsCount)
            put(Table.RATING_GOOD_REVIEW, film.ratingGoodReview)
            put(Table.RATING_GOOD_REVIEW_VOTE_COUNT, film.ratingGoodReviewVoteCount)
            put(Table.RATING, film.rating)
            put(Table.RATING_MPAA, film.ratingMPAA)
            put(Table.RATING_VOTE_COUNT, film.ratingVoteCount)
            put(Table.RATING_AWAIT, film.ratingAwait)
            put(Table.RATING_AWAIT_COUNT, film.ratingAwaitCount)
            put(Table.RATING_IMDB, film.ratingIMDb)
            put(Table.RATING_IMDB_VOTE_COUNT, film.ratingIMDbVoteCount)
            put(Table.DISTRIBUTORS, film.distributors)
            put(Table.PREMIERE_RU, film.premiereRu)
            put(Table.PREMIERE_WORLD, film.premiereWorld)
            put(Table.PREMIERE_WORLD_COUNTRY, film.premiereWorldCountry)
            put(Table.GROSS_RU, film.grossRu)
            put(Table.GROSS_USA, film.grossUsa)
            put(Table.GROSS_WORLD, film.grossWorld)
            put(Table.BUDGET, film.budget)
            put(Table.GALLERY, film.gallery?.joinToString(","))
            put(Table.TRIVIA_DATA, film.triviaData?.joinToString(","))
            put(Table.CREATORS, film.creators?.map { it.id }?.joinToString(","))
        }

        override fun mapToUpdateQuery(film: Film) = UpdateQuery.builder()
                .table(Table.TABLE_NAME)
                .where("${Table.ID} = ${film.id}")
                .build()

        override fun mapToInsertQuery(film: Film) = InsertQuery.builder()
                .table(Table.TABLE_NAME)
                .build()
    }

    class RelationsGetResolver : GetResolver<Film>() {
        val storio = ThreadLocal<StorIOSQLite>()

        override fun mapFromCursor(cursor: Cursor): Film {
            val creatorsIds = cursor.getString(cursor.getColumnIndex(Table.CREATORS))
            val creators = if (creatorsIds != null) {
                storio.get().get()
                        .listOfObjects(Profile::class.java)
                        .withQuery(Query.builder().table(Profile.Table.TABLE_NAME).where("${Profile.Table.ID} IN ($creatorsIds)").build())
                        .prepare()
                        .executeAsBlocking()
            } else null
            return Film(
                    id = cursor.getInt(cursor.getColumnIndex(Table.ID)),
                    nameRu = cursor.getString(cursor.getColumnIndex(Table.NAME_RU)),
                    nameEn = cursor.getString(cursor.getColumnIndex(Table.NAME_EN)),
                    year = cursor.getInt(cursor.getColumnIndex(Table.YEAR)),
                    cinemasCount = cursor.getInt(cursor.getColumnIndex(Table.CINEMAS_COUNT)),
                    posterBig = cursor.getString(cursor.getColumnIndex(Table.POSTER_BIG)),
                    posterSmall = cursor.getString(cursor.getColumnIndex(Table.POSTER_SMALL)),
                    filmLength = cursor.getString(cursor.getColumnIndex(Table.FILM_LENGTH)),
                    country = cursor.getString(cursor.getColumnIndex(Table.COUNTRY)),
                    genres = cursor.getString(cursor.getColumnIndex(Table.GENRES)).split(","),
                    desc = cursor.getString(cursor.getColumnIndex(Table.DESC)),
                    videoUrl = cursor.getString(cursor.getColumnIndex(Table.VIDEO_URL)),
                    reviewsCount = cursor.getInt(cursor.getColumnIndex(Table.REVIEWS_COUNT)),
                    ratingGoodReview = cursor.getFloat(cursor.getColumnIndex(Table.RATING_GOOD_REVIEW)),
                    ratingGoodReviewVoteCount = cursor.getInt(cursor.getColumnIndex(Table.RATING_GOOD_REVIEW_VOTE_COUNT)),
                    rating = cursor.getFloat(cursor.getColumnIndex(Table.RATING)),
                    ratingMPAA = cursor.getString(cursor.getColumnIndex(Table.RATING_MPAA)),
                    ratingVoteCount = cursor.getInt(cursor.getColumnIndex(Table.RATING_VOTE_COUNT)),
                    ratingAwait = cursor.getFloat(cursor.getColumnIndex(Table.RATING_AWAIT)),
                    ratingAwaitCount = cursor.getInt(cursor.getColumnIndex(Table.RATING_AWAIT_COUNT)),
                    ratingIMDb = cursor.getFloat(cursor.getColumnIndex(Table.RATING_IMDB)),
                    ratingIMDbVoteCount = cursor.getInt(cursor.getColumnIndex(Table.RATING_IMDB_VOTE_COUNT)),
                    distributors = cursor.getString(cursor.getColumnIndex(Table.DISTRIBUTORS)),
                    premiereRu = cursor.getString(cursor.getColumnIndex(Table.PREMIERE_RU)),
                    premiereWorld = cursor.getString(cursor.getColumnIndex(Table.PREMIERE_WORLD)),
                    premiereWorldCountry = cursor.getString(cursor.getColumnIndex(Table.PREMIERE_WORLD_COUNTRY)),
                    grossRu = cursor.getInt(cursor.getColumnIndex(Table.GROSS_RU)),
                    grossUsa = cursor.getInt(cursor.getColumnIndex(Table.GROSS_USA)),
                    grossWorld = cursor.getInt(cursor.getColumnIndex(Table.GROSS_WORLD)),
                    budget = cursor.getInt(cursor.getColumnIndex(Table.BUDGET)),
                    gallery = cursor.getString(cursor.getColumnIndex(Table.GALLERY))?.split(","),
                    triviaData = cursor.getString(cursor.getColumnIndex(Table.TRIVIA_DATA))?.split(","),
                    creators = creators
            )
        }

        override fun performGet(storio: StorIOSQLite, rawQuery: RawQuery): Cursor {
            this.storio.set(storio)
            return storio.lowLevel().rawQuery(rawQuery)
        }

        override fun performGet(storio: StorIOSQLite, query: Query): Cursor {
            this.storio.set(storio)
            return storio.lowLevel().query(query)
        }
    }

    class SimpleDeleteResolver : DefaultDeleteResolver<Film>() {
        override fun mapToDeleteQuery(film: Film) = DeleteQuery.builder()
                .table(Table.TABLE_NAME)
                .where("${Table.ID} = ${film.id}")
                .build()
    }

    object Table {
        val TABLE_NAME = "FILMS"
        val ID = "ID"
        val NAME_RU = "NAME_RU"
        val NAME_EN = "NAME_EN"
        val YEAR = "YEAR"
        val CINEMAS_COUNT = "CINEMAS_COUNT"
        val POSTER_BIG = "POSTER_BIG"
        val POSTER_SMALL = "POSTER_SMALL"
        val FILM_LENGTH = "FILM_LENGTH"
        val COUNTRY = "COUNTRY"
        val GENRES = "GENRES"
        val DESC = "DESC"
        val VIDEO_URL = "VIDEO_URL"
        val REVIEWS_COUNT = "REVIEWS_COUNT"
        val RATING_GOOD_REVIEW = "RATING_GOOD_REVIEW"
        val RATING_GOOD_REVIEW_VOTE_COUNT = "RATING_GOOD_REVIEW_VOTE_COUNT"
        val RATING = "RATING"
        val RATING_MPAA = "RATING_MPAA"
        val RATING_VOTE_COUNT = "RATING_VOTE_COUNT"
        val RATING_AWAIT = "RATING_AWAIT"
        val RATING_AWAIT_COUNT = "RATING_AWAIT_COUNT"
        val RATING_IMDB = "RATING_IMDB"
        val RATING_IMDB_VOTE_COUNT = "RATING_IMDB_VOTE_COUNT"
        val DISTRIBUTORS = "DISTRIBUTORS"
        val PREMIERE_RU = "PREMIERE_RU"
        val PREMIERE_WORLD = "PREMIERE_WORLD"
        val PREMIERE_WORLD_COUNTRY = "PREMIERE_WORLD_COUNTRY"
        val GROSS_RU = "GROSS_RU"
        val GROSS_USA = "GROSS_USA"
        val GROSS_WORLD = "GROSS_WORLD"
        val BUDGET = "BUDGET"
        val GALLERY = "GALLERY"
        val TRIVIA_DATA = "TRIVIA_DATA"
        val CREATORS = "CREATORS"
    }
}

data class Films(val films: List<Film>) {
    companion object {
        @JvmStatic fun deserializer() = Deserializer()
    }

    class Deserializer : JsonDeserializer<Films> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext) =
                Films(json.asJsonObject
                        .get("filmsData")
                        .asJsonArray
                        .map { context.deserialize<Film>(it, Film::class.java) })
    }
}