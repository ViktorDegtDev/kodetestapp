package kode.kinopoisk.bakurov.models

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.*
import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping
import com.pushtorefresh.storio.sqlite.operations.delete.DefaultDeleteResolver
import com.pushtorefresh.storio.sqlite.operations.get.DefaultGetResolver
import com.pushtorefresh.storio.sqlite.operations.put.DefaultPutResolver
import com.pushtorefresh.storio.sqlite.queries.*
import kode.kinopoisk.bakurov.models.Profile.Table.DESCRIPTION
import kode.kinopoisk.bakurov.models.Profile.Table.ID
import kode.kinopoisk.bakurov.models.Profile.Table.NAME_EN
import kode.kinopoisk.bakurov.models.Profile.Table.NAME_RU
import kode.kinopoisk.bakurov.models.Profile.Table.POSTER_URL
import kode.kinopoisk.bakurov.models.Profile.Table.PROFESSION_KEY
import kode.kinopoisk.bakurov.models.Profile.Table.PROFESSION_TEXT
import kode.kinopoisk.bakurov.models.Profile.Table.TABLE_NAME
import java.lang.reflect.Type

data class Profile(
        //region Fields
        val id: Int,
        val nameRu: String,
        val nameEn: String,
        val description: String?,
        val posterUrl: String?,
        val professionText: String,
        val professionKey: String
        //endregion
) : Parcelable {
    companion object {
        @JvmStatic fun deserializer() = Deserializer()
        @JvmStatic fun createTable(database: SQLiteDatabase) {
            database.execSQL("CREATE TABLE $TABLE_NAME (" +
                    "$ID INTEGER PRIMARY KEY," +
                    "$NAME_RU TEXT," +
                    "$NAME_EN TEXT," +
                    "$DESCRIPTION TEXT," +
                    "$POSTER_URL TEXT," +
                    "$PROFESSION_TEXT TEXT," +
                    "$PROFESSION_KEY TEXT" +
                    ");")
        }

        @JvmStatic fun storioTypeMapping() = SQLiteTypeMapping.builder<Profile>()
                .putResolver(SimplePutResolver())
                .getResolver(SimpleGetResolver())
                .deleteResolver(SimpleDeleteResolver())
                .build()

        @JvmField val CREATOR = object : Parcelable.Creator<Profile> {
            override fun createFromParcel(source: Parcel) = Profile(
                    id = source.readInt(),
                    nameRu = source.readString(),
                    nameEn = source.readString(),
                    description = source.readSerializable() as? String,
                    posterUrl = source.readString(),
                    professionText = source.readString(),
                    professionKey = source.readString()
            )

            override fun newArray(size: Int): Array<Profile?> = arrayOfNulls(size)
        }
    }

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(id)
        dest.writeString(nameRu)
        dest.writeString(nameEn)
        dest.writeSerializable(description)
        dest.writeString(posterUrl)
        dest.writeString(professionText)
        dest.writeString(professionKey)
    }

    class Deserializer : JsonDeserializer<Profile> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext) = json.asJsonObject.let {
            Profile(
                    id = it["id"].asInt,
                    nameRu = it["nameRU"].asString,
                    nameEn = it["nameEN"].asString,
                    description = it["description"]?.asString,
                    posterUrl = "https://kinopoisk.ru/images/${it["posterURL"]?.asString ?: "no-poster.gif"}",
                    professionText = it["professionText"].asString,
                    professionKey = it["professionKey"].asString
            )
        }
    }

    class SimplePutResolver : DefaultPutResolver<Profile>() {
        override fun mapToUpdateQuery(profile: Profile) = UpdateQuery.builder()
                .table(TABLE_NAME)
                .where("$ID = ${profile.id}")
                .build()

        override fun mapToInsertQuery(profile: Profile) = InsertQuery.builder()
                .table(TABLE_NAME)
                .build()

        override fun mapToContentValues(profile: Profile) = ContentValues().apply {
            put(ID, profile.id)
            put(NAME_RU, profile.nameRu)
            put(NAME_EN, profile.nameEn)
            put(DESCRIPTION, profile.description)
            put(POSTER_URL, profile.posterUrl)
            put(PROFESSION_TEXT, profile.professionText)
            put(PROFESSION_KEY, profile.professionKey)
        }
    }

    class SimpleGetResolver : DefaultGetResolver<Profile>() {
        override fun mapFromCursor(cursor: Cursor) = Profile(
                id = cursor.getInt(cursor.getColumnIndex(ID)),
                nameRu = cursor.getString(cursor.getColumnIndex(NAME_RU)),
                nameEn = cursor.getString(cursor.getColumnIndex(NAME_EN)),
                description = cursor.getString(cursor.getColumnIndex(DESCRIPTION)),
                posterUrl = cursor.getString(cursor.getColumnIndex(POSTER_URL)),
                professionText = cursor.getString(cursor.getColumnIndex(PROFESSION_TEXT)),
                professionKey = cursor.getString(cursor.getColumnIndex(PROFESSION_KEY))
        )
    }

    class SimpleDeleteResolver : DefaultDeleteResolver<Profile>() {
        override fun mapToDeleteQuery(profile: Profile) = DeleteQuery.builder()
                .table(TABLE_NAME)
                .where("$ID = ${profile.id}")
                .build()
    }

    object Table {
        val TABLE_NAME = "TABLE_NAME"
        val ID = "ID"
        val NAME_RU = "NAME_RU"
        val NAME_EN = "NAME_EN"
        val DESCRIPTION = "DESCRIPTION"
        val POSTER_URL = "POSTER_URL"
        val PROFESSION_TEXT = "PROFESSION_TEXT"
        val PROFESSION_KEY = "PROFESSION_KEY"
    }
}